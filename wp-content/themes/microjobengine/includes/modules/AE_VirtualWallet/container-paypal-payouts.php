<?php

/**
 * Class render order list in engine themes backend
 * - list order
 * - search order
 * - load more order
 * @since 1.0
 * @author Dakachi
 */
class AE_PaypalPayoutsContainer {

    /**
     * construct a user container
     */
    function __construct( $args = array(), $roles = '' ) {
        $this->args		=	$args;
        $this->roles	=	$roles;
    }
    /**
     *
     */
    function render()
    {
        $payouts = get_paypal_payouts();
        ?>
        <div class="et-main-content order-container paypal-payout-containerr">
            <div class="et-main-main">
                <div class="group-wrapper">
                    <div class="group-fields">
                        <div class="search-box et-member-search">
                            <p class="title-search" style="position: relative;"><?php _e("All Paypal Payouts", 'enginethemes'); ?></p>
                        </div>
                        <div class="et-main-main no-margin clearfix overview list fre-credit-withdraw-list-wrapper">
                            <div  class="list-payment-package">
                                <!-- order list  -->
                                <ul class="row title-list">
                                    <li class="col-md-1 col-sm-1"><?php _e('Withdraw ID', 'enginethemes')?></li>
                                    <li class="col-md-2 col-sm-2">
                                     <?php _e('Payout ID')?>
                                    </li>
                                    <li class="col-md-5 col-sm-5"><?php _e('Content', 'enginethemes'); ?></li>
                                    <li class="col-md-2 col-sm-2"><?php _e('Status')?></li>
                                    <li class="col-md-2 col-sm-2 time-request">
                                        <?php _e('Time requested', 'enginethemes'); ?>
                                    </li>
                                </ul>
                                <ul class="list-inner list-payment list-payouts payouts-list"
                                >
                                    <?php
                                    if ($payouts->have_posts()) {
                                        global $post, $ae_post_factory;
                                        $payout_obj = $ae_post_factory->get('ae_paypal_payout');
                                        while ($payouts->have_posts()) {
                                            $payouts->the_post();
                                            $payout = $payout_obj->convert($post);
                                            ?>
                                            <li class="row">
                                                <div class="col-md-1 col-sm-1"><?php echo $payout->withdraw_id?></div>
                                                <div class="col-md-2 col-sm-2"><?php echo $payout->p_id?></div>
                                                <div class="col-md-5 col-sm-5"><?php echo $payout->unfiltered_content ?></div>
                                                <div class="col-md-2 col-sm-2">
                                                    <span class="payout-status"  data-post-id="<?php echo $post->ID?>" data-payout-status><?php echo $payout->status?></span>
                                                    <a href="javascript;" class="update-payout" data-post-id="<?php echo $post->ID?>" data-payout-id="<?php echo $payout->p_id?>">Update</a>
                                                </div>
                                                <div class="col-md-2 col-sm-2 time-request">
                                                    <span class="date"><?php echo date(get_option('date_format').' '.get_option('time_format'),$payout->post_date).' '.mje_text_timezone(); //$withdraw->time_request_formated; ?></span>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    } else {
                                        _e('<li><p class="no-items">There are no Paypal payouts yet.</p></li>', 'enginethemes');
                                    } ?>
                                </ul>
                                <div class="col-md-12">
                                    <div class="paginations-wrapper">
                                        <?php
                                        ae_pagination($payouts, get_query_var('paged'), 'page');
                                        wp_reset_query();
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
}

function get_paypal_payouts($args = array()){
    $default_args = array(
        'paged' => 1,
        'post_status' => array(
            'pending',
            'publish',
            'draft'
        ),
    );
    $args = wp_parse_args($args, $default_args);
    $args['post_type'] = 'ae_paypal_payout';
    $payout_query = new WP_Query($args);
    return $payout_query;
}

